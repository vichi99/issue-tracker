#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color

# Give some time to kick off to postgres and to load dump.
./wait_for_it.sh db:5432 --timeout=120 --strict  -- echo "postgres is up"

# Run migrations.
if ./manage.py migrate; then
    echo -e "${GREEN}Database migrated.${NC}"
else
    echo -e "${RED}Couldn't migrate the database.${NC}"
    exit 1
fi

Fixtures.
echo -e "${GREEN}Loading fixtures.${NC}"
if ./manage.py loaddata groups users categories issues; then
    echo -e "${GREEN}Fixtures loaded.${NC}"
else
    echo -e "${RED}Couldn't load fixtures.${NC}"
    exit 1
fi

# Run the app.
echo -e "${GREEN}Runing the app at localhost:8000${NC}"
python manage.py runserver 0.0.0.0:8000
