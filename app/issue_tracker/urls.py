from django.contrib import admin
from django.urls import include, path
from rest_framework.schemas import get_schema_view

urlpatterns = [
    path("", include("issue_tracker.core.urls", namespace="core")),
    path("admin/", admin.site.urls),
    path("issues/", include("issue_tracker.issues.urls", namespace="issues")),
    path(
        "openapi",
        get_schema_view(
            title="Issue tracker",
            description="API for issues",
            version="1.0.0",
        ),
        name="openapi-schema",
    ),
]
