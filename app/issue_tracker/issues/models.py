from datetime import timedelta
from enum import Enum, unique

from django.db import models
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True, verbose_name="name")

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Issue(models.Model):
    """
    Issues as on github and gitlab
    """

    @unique
    class StateEnum(Enum):
        TODO = "Scheduled"
        IN_PROGRESS = "In progress"
        MERGED = "Merge/Pull request done"
        TESTED_OK = "Tested ok"
        TESTED_NOK = "Tested nok"
        COMPLETE = "Complete"

    STATE_CHOICES = [
        (StateEnum.TODO.name, StateEnum.TODO.value),
        (StateEnum.IN_PROGRESS.name, StateEnum.IN_PROGRESS.value),
        (StateEnum.MERGED.name, StateEnum.MERGED.value),
        (StateEnum.TESTED_OK.name, StateEnum.TESTED_OK.value),
        (StateEnum.TESTED_NOK.name, StateEnum.TESTED_NOK.value),
        (StateEnum.COMPLETE.name, StateEnum.COMPLETE.value),
    ]

    name = models.CharField(unique=True, max_length=128, verbose_name="name")
    autor = models.ForeignKey(
        "users.User",
        related_name="autors",
        on_delete=models.CASCADE,
        verbose_name="issue autor",
    )
    solver = models.ForeignKey(
        "users.User",
        related_name="solvers",
        on_delete=models.CASCADE,
        verbose_name="issue solver",
    )
    description = models.TextField(default="", verbose_name="description")
    state = models.CharField(
        max_length=50, choices=STATE_CHOICES, verbose_name="state"
    )
    category = models.ForeignKey(
        "issues.Category",
        on_delete=models.CASCADE,
        verbose_name="category",
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name="created at"
    )
    completed_at = models.DateTimeField(
        blank=True, null=True, verbose_name="completed at"
    )
    duration = models.PositiveIntegerField(
        verbose_name="solution time [s]",
        blank=True,
        null=True,
        editable=False,
    )

    @property
    def is_issue_done(self) -> bool:
        """
        The issue is completed if the state is completed.

        :return: Returns whether the issue is completed.
        :rtype: bool
        """
        return self.state == self.StateEnum.COMPLETE.name

    @property
    def get_duration_human_readable(self) -> str:
        """
        Human readable time solution.

        :return: Returns the human readable time solution of the issue.
        :rtype: str
        """
        if (
            self.created_at
            and self.completed_at
            and (self.created_at - self.completed_at).seconds
        ):
            diff: timedelta = self.completed_at - self.created_at
            return ":".join(str(diff).split(":")[:2])
        return ""

    def save(self, *args, **kwargs):
        # save time and duration if the issue is completed
        try:
            # when editing an existing condition
            issue_old: Issue = Issue.objects.get(pk=self.pk)
            # if self.is_issue_done:
            if not issue_old.is_issue_done and self.is_issue_done:
                # condition only when the last state is not done
                # and the current state is done

                self.completed_at = timezone.now()
                # calculate saconds to the duration field
                diff: timedelta = self.completed_at - self.created_at
                duration = (diff.days * 24 * 60 * 60) + diff.seconds
                if duration and duration != self.duration:
                    # inserts only when changing
                    self.duration = duration
        except Issue.DoesNotExist:
            # when creating a new issue
            if self.is_issue_done:
                self.created_at = self.completed_at = timezone.now()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Issue"
        verbose_name_plural = "Issues"

    def __str__(self):
        return self.name
