# Generated by Django 4.0.5 on 2022-07-03 19:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0003_issue_solver'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='autor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='autors', to='users.User', verbose_name='issue autor'),
        ),
    ]
