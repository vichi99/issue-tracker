from rest_framework import status
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from issue_tracker.issues.models import Issue
from issue_tracker.issues.serializers import IssueSerializer


class IssueListApiView(APIView):
    def get(self, request, *args, **kwargs):
        """
        List all the issues items.
        """
        issues = Issue.objects.all()
        serializer = IssueSerializer(issues, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class IssueDetailApiView(RetrieveAPIView):
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer
