from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from issue_tracker.issues.models import Category, Issue
from issue_tracker.issues.serializers import IssueSerializer
from issue_tracker.users.models import User


class IssueApiTests(APITestCase):
    def setUp(self):
        user = User.objects.create(
            username="john_blue",
            first_name="John",
            last_name="Blue",
        )
        category = Category.objects.create(name="Bug")

        Issue.objects.create(
            name="test issue",
            autor=user,
            solver=user,
            description="description",
            state=Issue.StateEnum.IN_PROGRESS.name,
            category=category,
        )
        Issue.objects.create(
            name="test issue 2",
            autor=user,
            solver=user,
            description="description",
            state=Issue.StateEnum.MERGED.name,
            category=category,
        )

    def test_get_valid_list(self):
        """
        Test issues valid list.
        """
        response = self.client.get(reverse("issues:list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Issue.objects.count(), len(response.data))
        self.assertEqual(
            response.data,
            IssueSerializer(Issue.objects.all(), many=True).data,
        )

    def test_get_valid_single_detail(self):
        """
        Test issue valid detail.
        """
        issue_pk = Issue.objects.first().pk
        url = reverse("issues:detail", kwargs={"pk": issue_pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            IssueSerializer(Issue.objects.get(pk=issue_pk)).data,
        )

    def test_get_invalid_single_detail(self):
        """
        Test issue invalid detail.
        """
        url = reverse("issues:detail", kwargs={"pk": 999})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
