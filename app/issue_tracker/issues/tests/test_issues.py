import time
from datetime import timedelta

from django.db.utils import IntegrityError
from django.test import TestCase
from django.utils import timezone

from issue_tracker.issues.models import Category, Issue
from issue_tracker.users.models import User


class IssueTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username="john_blue",
            first_name="John",
            last_name="Blue",
        )
        self.category = Category.objects.create(name="Bug")

        self.issue_progress = Issue.objects.create(
            name="test issue",
            autor=self.user,
            solver=self.user,
            description="description",
            state=Issue.StateEnum.IN_PROGRESS.name,
            category=self.category,
        )
        self.issue_complete = Issue.objects.create(
            name="test issue 2",
            autor=self.user,
            solver=self.user,
            description="description",
            state=Issue.StateEnum.COMPLETE.name,
            category=self.category,
        )

    def test_category_unique(self):
        """
        Test whether the category is unique.
        """
        with self.assertRaises(IntegrityError):
            Category.objects.create(name="Bug")

    def test_issue_unique(self):
        """
        Test whether the issue is unique.
        """
        with self.assertRaises(IntegrityError):
            Issue.objects.create(
                name="test issue",
                autor=self.user,
                solver=self.user,
                description="description",
                state=Issue.StateEnum.IN_PROGRESS.name,
                category=self.category,
            )

    def test_issue_state(self):
        """
        Test whether the issue is done only when the state is COMPLETE.
        """
        for i in Issue.STATE_CHOICES:
            self.issue_progress.state = i[0]
            if i[0] == Issue.StateEnum.COMPLETE.name:
                self.assertTrue(self.issue_progress.is_issue_done)
            else:
                self.assertFalse(self.issue_progress.is_issue_done)

    def test_issue_duration(self):
        """
        Test duration when creating complete issue.
        """
        self.assertIsNone(self.issue_complete.duration)
        self.assertEqual(
            (
                self.issue_complete.created_at
                - self.issue_complete.completed_at
            ).seconds,
            0,
        )

    def test_issue_duration_diff(self):
        """
        Test if the difference between creation time and end time fits.
        """
        self.issue_progress.state = Issue.StateEnum.COMPLETE.name
        self.issue_progress.created_at = timezone.now() - timedelta(seconds=60)
        self.issue_progress.save()
        diff = self.issue_progress.completed_at - self.issue_progress.created_at
        self.assertEqual(
            self.issue_progress.duration,
            (diff.days * 24 * 60 * 60) + diff.seconds,
        )

    def test_issue_editing_if_complete(self):
        """
        Test the duration when editing the completed issue.
        """
        duration_old = self.issue_complete.duration
        self.issue_complete.description = "changed"
        time.sleep(1)
        self.issue_complete.save()
        duration_new = self.issue_complete.duration
        self.assertEqual(duration_old, duration_new)
