from django.apps import AppConfig


class IssueConfig(AppConfig):
    name = "issue_tracker.issues"
    verbose_name = "Issues"
