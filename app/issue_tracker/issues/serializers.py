from issue_tracker.core.serializers import BaseSerializer
from issue_tracker.issues.models import Issue


class IssueSerializer(BaseSerializer):
    class Meta:
        model = Issue
        fields = [
            "pk",
            "name",
            "autor",
            "solver",
            "description",
            "state",
            "category",
            "created_at",
            "completed_at",
            "duration",
        ]
