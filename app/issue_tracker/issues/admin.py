from datetime import timedelta

from django.contrib import admin
from django.db.models import Avg, Max, Min, Sum

from issue_tracker.issues.models import Category, Issue


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    change_list_template = "admin/issues_change_list.html"
    list_display = [
        "name",
        "autor",
        "solver",
        "state",
        "category",
        "created_at",
        "completed_at",
        "duration_human_readable",
        "duration",
    ]
    search_fields = ["name", "autor", "solver", "description"]
    fields = [
        "name",
        "autor",
        "solver",
        "description",
        "state",
        "category",
        "created_at",
        "completed_at",
        "duration",
    ]
    readonly_fields = ["created_at", "completed_at", "duration"]
    list_filter = ["autor", "solver", "state", "category"]
    ordering = ["created_at"]

    def duration_human_readable(self, obj: Issue):
        return obj.get_duration_human_readable

    duration_human_readable.short_description = "solution time"

    def seconds_to_human_readable(self, seconds: float) -> str:
        """
        Convert seconds to human readable time.

        :return: Returns the human readable time from seconds.
        :rtype: str
        """
        if seconds:
            diff = timedelta(seconds=int(seconds))
            return ":".join(str(diff).split(":")[:2])
        else:
            return None

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        res = self.model.objects.all().aggregate(
            Min("duration"),
            Max("duration"),
            Avg("duration"),
            Sum("duration"),
        )
        extra_context["duration_min"] = self.seconds_to_human_readable(
            res["duration__min"]
        )
        extra_context["duration_max"] = self.seconds_to_human_readable(
            res["duration__max"]
        )
        extra_context["duration_avg"] = self.seconds_to_human_readable(
            res["duration__avg"]
        )
        extra_context["duration_sum"] = self.seconds_to_human_readable(
            res["duration__sum"]
        )
        return super().changelist_view(request, extra_context=extra_context)
