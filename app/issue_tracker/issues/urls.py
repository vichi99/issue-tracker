from django.urls import path

from issue_tracker.issues.views import IssueDetailApiView, IssueListApiView

app_name = "issues"
urlpatterns = [
    path("api/", IssueListApiView.as_view(), name="list"),
    path("api/<int:pk>/", IssueDetailApiView.as_view(), name="detail"),
]
