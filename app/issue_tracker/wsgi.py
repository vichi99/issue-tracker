import os

from django.core.wsgi import get_wsgi_application

if "true" == os.environ.get("DJANGO_DEBUG"):
    settings = "issue_tracker.settings.dev"
else:
    settings = "issue_tracker.settings.base"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings)

application = get_wsgi_application()
