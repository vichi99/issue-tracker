from django.urls import path

from issue_tracker.core.views import AdminRedirectView

app_name = "core"
urlpatterns = [
    path("", AdminRedirectView.as_view(), name="homepage"),
]
