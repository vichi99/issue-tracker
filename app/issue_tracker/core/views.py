import logging

from django.views.generic import RedirectView

LOGGER = logging.getLogger(__name__)


class AdminRedirectView(RedirectView):
    url = "admin/"
