from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "issue_tracker.core"
    verbose_name = "Issue tracker"
