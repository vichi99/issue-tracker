import logging

from django.contrib.auth.models import AbstractUser, Group

LOGGER = logging.getLogger(__name__)


class User(AbstractUser):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """

    def __str__(self):
        if self.first_name and self.last_name:
            return " ".join([self.first_name, self.last_name])
        return super().__str__()

    def save(self, *args, **kwargs):
        # When there is a user who is not a superuser
        # and is a staff assigns a readonly group.
        if self.id and self.is_staff and not self.is_superuser:
            try:
                group = Group.objects.get(name="readonly")
            except Group.DoesNotExist as e:
                LOGGER.exception(e, exc_info=True)
            else:
                if group not in self.groups.all():
                    self.groups.add(group)

        super().save(*args, **kwargs)
