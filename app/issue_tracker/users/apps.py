from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "issue_tracker.users"
    verbose_name = "Users"
