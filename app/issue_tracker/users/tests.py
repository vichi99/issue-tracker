from django.contrib.auth.models import Group
from django.test import TestCase

from issue_tracker.users.models import User


class UsersTestCase(TestCase):
    def setUp(self):
        self.group = Group.objects.create(name="readonly")
        self.staff = User.objects.create(
            username="john_blue",
            first_name="John",
            last_name="Blue",
            is_superuser=False,
            is_staff=True,
        )
        self.superuser = User.objects.create(
            username="joe_olin",
            first_name="Joey",
            last_name="Olin",
            is_superuser=True,
            is_staff=True,
        )

    def test_user_group(self):
        """
        Test whether the user has a group of permissions when creating.
        """
        self.staff.save()
        self.superuser.save()
        self.assertTrue(self.group in self.staff.groups.all())
        self.assertFalse(self.group in self.superuser.groups.all())
