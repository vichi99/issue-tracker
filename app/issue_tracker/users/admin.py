import logging

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from issue_tracker.users.models import User

LOGGER = logging.getLogger(__name__)


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_display = [
        "username",
        "first_name",
        "last_name",
        "is_staff",
        "is_superuser",
        "is_active",
    ]
    readonly_fields = ["date_joined", "last_login"]

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        is_superuser = request.user.is_superuser
        disabled_fields = []

        if not is_superuser:
            disabled_fields += ["is_superuser", "user_permissions"]

        # Prevent non-superusers from editing their own permissions
        if not is_superuser and obj is not None and obj == request.user:
            disabled_fields += [
                "is_staff",
                "is_superuser",
                "groups",
                "user_permissions",
            ]

        for f in disabled_fields:
            if f in form.base_fields:
                form.base_fields[f].disabled = True

        return form
