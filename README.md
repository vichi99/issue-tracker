# Issue Tracker (side project)

[![Pipeline Status](https://gitlab.com/vichi99/issue-tracker/badges/master/pipeline.svg)](https://gitlab.com/vichi99/issue-tracker/-/commits/master)
[![Python version](https://img.shields.io/badge/python-3.10-blue.svg)](https://www.python.org/downloads/release/python-3100)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/vichi99/issue-tracker/-/blob/master/LICENSE)
[![Python Black Code Style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)

Issue tracker to help manage tickets. Tickets can be managed in
django admin or view for list and detail in api.

For Issue it is possible to set the name, author, solver, description, state and category.
Values as created at, completed at and duration are set automatically.
The duration time is calculated once the ticket is marked as completed.

The project contains several test issues, users, categories and group in `fixtures`.

![admin issues](images/admin_issues.png)

Several users, the first of which is superuser with login details:

```
username: admin
password: admin
```

Other users are staff who only have rights to view admin.
Their username is in the first column and they all have the same password.
When creating a staff account that is not superuser a group with
admin preview privileges is automatically added.

```
username: george_lane
password: Heslo12345
```

![admin users](images/admin_users.png)

Default group assigned to staff users.

![admin group](images/admin_group.png)

The project includes an api where you can load the issues list

![admin issue list](images/api_issue_list.png)
or issue detail.
![admin issue detail](images/api_issue_detail.png)

# Requirements

- [docker, docker-compose](https://www.docker.com/)
- Clone project
  ```sh
  $ git clone https://gitlab.com/vichi99/issue-tracker.git
  ```
- [poetry](https://python-poetry.org/) - optionally, for local IDE and adding pip packages

  ```sh
  $ pip install poetry
  $ poetry install
  $ poetry add package_name # for adding package
  ```

- [pre-commit](https://pre-commit.com/) - optionally
  ```sh
  $ poetry add pre-commit # or pip install pre-commit
  $ pre-commit install # install locally to a git directory
  ```

# Quickstart

```sh
$ docker-compose up # start container
$ docker-compose down -v # for stop and remove containers and remove volumes
$ docker-compose build # for rebuild docker image (etc. if packages are added)
```

**web should be on [http://localhost:8000](http://localhost:8000)**

Login to admin:

```sh
# as superuser
username: admin
password: admin
# as staff
username: george_lane
password: Heslo12345
```

# API endpoints

These endpoints allow you to view list or detail of issue.

Django REST Framework OpenAPI detail [http://localhost:8000/openapi](http://localhost:8000/openapi)

## GET

`Issues list` - [/issues/api/](#get-issuesapi)<br/>
`Issue detail` - [/issues/api/[issue_pk]](#get-issuesapiissuepk)<br/>

---

### GET /issues/api/

Returns issue list.

**Successful response** - [http://localhost:8000/issues/api/](http://localhost:8000/issues/api/)

Status code: `200`

```json
[
    {
        "pk": 1,
        "name": "Data error",
        "autor": 6,
        "solver": 2,
        "description": "description",
        "state": "COMPLETE",
        "category": 1,
        "created_at": "2022-06-30T17:58:31.033000+02:00",
        "completed_at": "2022-06-30T20:58:12.232000+02:00",
        "duration": 10781
    },
    {
        "pk": 2,
        "name": "Missing tests",
        "autor": 2,
        "solver": 3,
        "description": "Please add some tests to the core.",
        "state": "COMPLETE",
        "category": 2,
        "created_at": "2022-06-30T18:04:07.331000+02:00",
        "completed_at": "2022-07-03T21:24:06.344000+02:00",
        "duration": 271199
    },
    ...
]
```

### GET /issues/api/issue_pk/

Returns issue detail.

**Parameters**

|       Name | Required |  Type   | Description                   |
| ---------: | :------: | :-----: | ----------------------------- |
| `issue_pk` | required | integer | The product pk from database. |

**Successful response** - [http://localhost:8000/issues/api/1/](http://localhost:8000/issues/api/1/)

Status code: `200`

```json
{
  "pk": 1,
  "name": "Data error",
  "autor": 6,
  "solver": 2,
  "description": "description",
  "state": "COMPLETE",
  "category": 1,
  "created_at": "2022-06-30T17:58:31.033000+02:00",
  "completed_at": "2022-06-30T20:58:12.232000+02:00",
  "duration": 10781
}
```

**Unsuccessful response**

Status code: `404`

```json
{
  "detail": "Not found."
}
```

---

# CI/CD

These steps must be done before pushing commit, otherwise the pipeline in gitlab will crash:

## unittests

```sh
$ docker exec -it issue_tracker ./manage.py test # if app is already running
```

## utilities

```sh
$ isort app
$ black app
$ flake8 app
```

You can automate these steps by installing the [pre-commit](https://pre-commit.com/). The project contains a script [.pre-commit-config.yaml](.pre-commit-config.yaml)

# License

The [MIT](LICENSE) License.
